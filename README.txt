CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Sometimes you want a menu to be permanently visually-collapsed into a button,
which you must click on in order to see the items inside that menu.

As of version 1.0, you can:

* Visually hide/show a menu when the end-user clicks on a glyph.
* Screen-readers see a fully-expanded menu and do not see the clickable glyph,
  to reduce confusion and save time by avoiding unnecessary interaction.
* Simple HTML, CSS, and JavaScript to make customization easier.

REQUIREMENTS
------------

This module requires the following modules:

* Block (in Drupal core)
* Menu (in Drupal core)

INSTALLATION
------------

1. Download and install the menu_button project.

   See https://www.drupal.org/docs/7/extend/installing-modules for further
   information.

2. Go to the block administration page at Administration -> Structure -> Blocks.
   Look for blocks titled "Menu button: $menu_name". Move the menu button blocks
   that correspond with the menus you want to display as buttons into the
   appropriate regions.

   See https://www.drupal.org/node/1576530 for further information.

CONFIGURATION
-------------

1. To change which character(s) the user has to click on to hide/show the menu,
   edit the block settings and change the "glyph" setting.

   See https://drupal.org/node/1576532 for further information.

MAINTAINERS
-----------

Current maintainers:

* M Parker (mparker17) - https://www.drupal.org/user/536298
* Luke Bainbridge (lbainbridge) - https://www.drupal.org/user/2406996

This project has been sponsored by:

* Digital Echidna (http://echidna.ca/)
